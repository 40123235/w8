i = 1
j = 1
while i <= 9:
    if i == 8:
        break
 
    while j <= 9:
        if j == 4:
            j += 1
            continue
 
        print(i * j, end = " ")
        j += 1
 
    i += 1
    j = 1
    print()
